CXX = g++
INCLUDES = -I/usr/include/
CXXFLAGS = -O3 $(INCLUDES)
LIBS =  -lopencv_highgui -lopencv_core -lopencv_imgproc

.SUFFIXES: .cc
.cc.o:
	$(CXX) $(CXXFLAGS) -c $<
	
#-----File Dependencies-----------

SRC = main.cpp eBugData.cpp

OBJ = $(addsuffix .o, $(basename $(SRC)))

all: main

main: main.o eBugData.o $(OBJ)
	 $(CXX) $(CXXFLAGS) -o main $(OBJ) $(LIBS)
	 
#---------Other stuff------------

depend:
	makedepend $(CXXFLAGS) -Y $(SRC)

clean:
	rm -f $(OBJ)

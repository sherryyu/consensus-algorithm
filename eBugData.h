/*
 * eBugData.h
 *
 *  Created on: May 22, 2013
 *      Author: sherry
 */

#ifndef EBUGDATA_H_
#define EBUGDATA_H_


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <map>
#include "utils.h"
#include <iostream>

using namespace cv;
using namespace std;

class eBugData{

public:
        eBugData(int eBug_id, Point eBug_Global, int eBug_angle);
        ~eBugData();

//        void setLeader();
//        void getLeader();
//        void setState();
//        void getState();

        vector<relativePos> neighboursPos;
        int leadersID[2];

        float get_distance(eBugData eBug);
        float get_angle(eBugData eBug);
        Point get_GlobalPos();
        float get_orientation();

//        int getId();
//        Point getPosition();
//        float getOrientation();
//        void setNeighbours(vector<eBugData> neighbours);
//        void getRelativePos(vector<eBugData> neighbours);

private:
//        vector<eBugData> getNeighbours();
        Point Global_pos;
        int id;
        float orientation;
        vector<eBugData> neighbours;



};


#endif /* EBUGDATA_H_ */

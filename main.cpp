/*
 * main.cpp
 *
 *  Created on: 13/05/2013
 *      Author: ebugmaster
 */
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include "eBugData.h"
#include "utils.h"
#include "config.h"

using namespace cv;
using namespace std;

int main(int argc, char** argv){
	// number of eBugs
	int n=4;

	// global eBug_global positions
	vector<Point> eBug_global(n);

	// Initial positions
	eBug_global[0].x=150;
	eBug_global[0].y=150;
	eBug_global[1].x=200;
	eBug_global[1].y=200;
	eBug_global[2].x=100;
	eBug_global[2].y=100;
	eBug_global[3].x=300;
	eBug_global[3].y=200;

	// distance to goal vector
	vector<int> dis2goal(n);

	// create a vector of eBug data objects
	vector<eBugData> eBugs;

	eBugData eBug1(1,eBug_global[1],0);
	eBugData eBug2(2,eBug_global[2],pi);
	eBugData eBug3(3,eBug_global[3],0);
	eBugData eBug0(0,eBug_global[0],pi/2);

	eBugs.push_back(eBug0);
	eBugs.push_back(eBug1);
	eBugs.push_back(eBug2);
	eBugs.push_back(eBug3);

//////////////////////////////////////////////////
////////// find every eBug's neighbours //////////
//////////////////////////////////////////////////

	relativePos temp;

	for (int i=0; i<n; i++){
		for (int j = i+1; j<n; j++)
		{
			if (eBugs[i].get_distance(eBugs[j]) <= SENSOR_RANGE){
				temp.id = j;
				temp.distance = eBugs[i].get_distance(eBugs[j]);
#if DEBUG
				cout << "distance" << j<<"to" << i<< " =" << temp.distance<< endl;
#endif
				temp.angle = eBugs[i].get_angle(eBugs[j]);
				eBugs[i].neighboursPos.push_back(temp);
				temp.id = i;
				temp.angle = eBugs[j].get_angle(eBugs[i]);
				eBugs[j].neighboursPos.push_back(temp);
			}
		}
	}
#if DEBUG
	cout << eBugs[0].neighboursPos.size()<<endl;
	cout << eBugs[1].neighboursPos.size()<<endl;
	cout << eBugs[2].neighboursPos.size()<<endl;
	cout << eBugs[3].neighboursPos.size()<<endl;
#endif
	//////////////////////////////////////////////////
	////////// assign leaders of each eBug ///////////
	//////////////////////////////////////////////////

	// calculate distance of each eBug to goal and select the closest one as leader
	int FL = 0;					// Formation Leader
	dis2goal[0]=sqrt(pow(GOAL_X-eBugs[0].get_GlobalPos().x,2)+pow(GOAL_Y-eBugs[0].get_GlobalPos().y,2));
	for (int i=1; i<n; i++){
		dis2goal[i]=sqrt(pow(GOAL_X-eBugs[i].get_GlobalPos().x,2)+pow(GOAL_Y-eBugs[i].get_GlobalPos().y,2));
		if(dis2goal[i]<=dis2goal[i-1]){
			FL = i;
		}
	}

	//////////// assign leaders to the rest  ///////////////

	// find Follower Robot 1 and Follower Robot 2
	int d_temp[eBugs[FL].neighboursPos.size()];				// distances to neighbours
	int FR1 = 0;
	int FR2 = 0;

	// calculate FL distances to neighbours
	for (int i=0; i<eBugs[FL].neighboursPos.size(); i++){
		d_temp[i] = eBugs[FL].get_distance(eBugs[eBugs[FL].neighboursPos[i].id]);
	}
	// nearest robot = FR1;
	for (int i=1; i<eBugs[FL].neighboursPos.size();i++){
		if (d_temp[i]<=d_temp[i-1]){
			FR1 = eBugs[FL].neighboursPos[i].id;
		}
	}
	eBugs[FR1].leadersID[0] = FL;
	// second nearest = FR2;
	for (int i=1; i<eBugs[FL].neighboursPos.size();i++){
		if ((d_temp[i]<=d_temp[i-1])&&(eBugs[FL].neighboursPos[i].id!= FR1)){
			FR2 = eBugs[FL].neighboursPos[i].id;
		}
	}
	eBugs[FR2].leadersID[0] = FL;
	eBugs[FR2].leadersID[1] = FR1;

	// the other robot is FR3
	int FR3;
	for (int i = 0; i<n; i++){
		if((i!=FL)&&(i!=FR1)&&(i!=FR2)){
			FR3 = i;
		}
	}



#if DEBUG
	cout <<"FL = "<<FL<<endl;
	cout <<"FR1 = "<<FR1<<endl;
	cout <<"FR2 = "<<FR2<<endl;
	cout <<"FR3 = "<<FR3<<endl;
#endif


/////////////////////////////////////////////////////
/////////////// Draw eBugs on the pic////////////////
/////////////////////////////////////////////////////



	Mat image(480,640, CV_8UC1, Scalar(0,0,0));

//	// event loop
//	for(int i=0;i<200;i++){
//		// reset eBug_global image
//		cv::circle(image,eBug_global[0],22,Scalar(0,0,0),1,8,0);
//		cv::circle(image,eBug_global[1],22,Scalar(0,0,0),1,8,0);
//		cv::circle(image,eBug_global[2],22,Scalar(0,0,0),1,8,0);

//
//		// calculate desired positions
//		normal[1]=(relative_ebug2[1].x-relative_ebug2[0].x)/sqrt(pow(relative_ebug2[1].x-relative_ebug2[0].x,2)+pow(relative_ebug2[1].y-relative_ebug2[0].y,2));
//		normal[0]=-(relative_ebug2[1].y-relative_ebug2[0].y)/sqrt(pow(relative_ebug2[1].x-relative_ebug2[0].x,2)+pow(relative_ebug2[1].y-relative_ebug2[0].y,2));
//
//		d= sqrt(pow(relative_ebug2[1].x-relative_ebug2[0].x,2)+pow(relative_ebug2[1].y-relative_ebug2[0].y,2));
//		ebug2_desired[0] = (relative_ebug2[0].x+relative_ebug2[1].x)/2+sqrt(3)/2*d*normal[0];
//		ebug2_desired[1] = (relative_ebug2[0].y+relative_ebug2[1].y)/2+sqrt(3)/2*d*normal[1];
//
//
//
//		// calculate speed
//		v_ebug2[0]=ebug2_desired[0]/30;
//		v_ebug2[1]=ebug2_desired[1]/30;
//
//		// move there
//		eBug_global[2].x += ceil(v_ebug2[0]);
//		eBug_global[2].y += ceil(v_ebug2[1]);
//
//

		// draw eBugs
		cv::circle(image,eBugs[FL].get_GlobalPos(),22,Scalar(255,255,255),1,8,0);
		Point orient;
		orient.x = eBugs[FL].get_GlobalPos().x+22*cos(eBugs[FL].get_orientation());
		orient.y = eBugs[FL].get_GlobalPos().y+22*sin(eBugs[FL].get_orientation());
		cv::line(image,eBugs[FL].get_GlobalPos(), orient,Scalar(255,255,255),1,8,0);
		cv::line(image,eBugs[FL].get_GlobalPos(), orient,Scalar(255,255,255),1,8,0);

		cv::circle(image,eBugs[FR1].get_GlobalPos(),22,Scalar(125,125,0),1,8,0);
		orient.x = eBugs[FR1].get_GlobalPos().x+22*cos(eBugs[FR1].get_orientation());
		orient.y = eBugs[FR1].get_GlobalPos().y+22*sin(eBugs[FR1].get_orientation());
		cv::line(image,eBugs[FR1].get_GlobalPos(), orient,Scalar(255,255,255),1,8,0);

		cv::circle(image,eBugs[FR2].get_GlobalPos(),22,Scalar(155,155,0),1,8,0);
		orient.x = eBugs[FR2].get_GlobalPos().x+22*cos(eBugs[FR2].get_orientation());
		orient.y = eBugs[FR2].get_GlobalPos().y+22*sin(eBugs[FR2].get_orientation());
		cv::line(image,eBugs[FR2].get_GlobalPos(), orient,Scalar(255,255,255),1,8,0);

		cv::circle(image,eBugs[FR3].get_GlobalPos(),22,Scalar(155,155,0),1,8,0);
		orient.x = eBugs[FR3].get_GlobalPos().x+22*cos(eBugs[FR3].get_orientation());
		orient.y = eBugs[FR3].get_GlobalPos().y+22*sin(eBugs[FR3].get_orientation());
		cv::line(image,eBugs[FR3].get_GlobalPos(), orient,Scalar(255,255,255),1,8,0);

		namedWindow("Display window", CV_WINDOW_AUTOSIZE);
		imshow("Display window", image);

		waitKey(10000);
			cin.get();
//	}
//	// close the window
//	cvDestroyWindow("Display window");
	return 0;

}


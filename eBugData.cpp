/*
 * eBugData.cpp
 *
 *  Created on: May 22, 2013
 *      Author: sherry
 */

#include"eBugData.h"

eBugData::eBugData(int eBug_id, Point eBug_Global, int eBug_orientation)
{
	id = eBug_id;
	orientation = eBug_orientation;
	Global_pos = eBug_Global;
	leadersID[0] =0;
	leadersID[1] =0;
}

eBugData::~eBugData(){}

//vector<eBugData> eBugData::getNeighbours()
//{
//
//}


float eBugData::get_distance(eBugData ebug){
	return sqrt(pow(Global_pos.x-ebug.Global_pos.x,2)+pow(Global_pos.y-ebug.Global_pos.y,2));
}

float eBugData::get_angle(eBugData ebug){
	float grad = atan2((ebug.Global_pos.y-Global_pos.y),(ebug.Global_pos.x-Global_pos.x));
	return orientation-grad;
}

Point eBugData::get_GlobalPos(){
	return Global_pos;
}

float eBugData::get_orientation(){
	return orientation;
}

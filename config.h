/*
 * config.h
 *
 *  Created on: May 23, 2013
 *      Author: sherry
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#define DEBUG										1
#define SENSOR_RANGE								200
#define GOAL_X										640
#define GOAL_Y										470
#define pi 											3.1415926



#endif /* CONFIG_H_ */
